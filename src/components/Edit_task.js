import React from 'react';

const Edit_task = ({EDIT}) => {
    return (
        <div>
            <button onClick={EDIT} style={{color:'purple'}}>
                Edit task
            </button>
        </div>
    );
};

export default Edit_task;