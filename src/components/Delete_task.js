import React from 'react';

const Delete_task = ({Delete}) => {
    return (
        <div>
            <button style={{color:'red'}} onClick={Delete}>
                Delete task
            </button>
        </div>
    );
};

export default Delete_task;