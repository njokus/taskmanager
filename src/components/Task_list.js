import React, { Component } from 'react';
import Edit_task from './Edit_task';
import Search from './Search' ;
import Delete_task from './Delete_task' ;
import Add_task from './Add_task';
import Task from './Task';


var sectionStyle = { width: "100%", height: "250px",marginTop:'100px' 
    , backgroundColor:'white',marginRight:'auto', overflow:'auto',marginLeft:'-38%'};



class Task_list extends Component {
  


    


   render() {

    return (

        <div  style={sectionStyle }>
           
               
                    
           <div style={{width:'60%'}}>
               
                    { this.props.tasks.map(task => <Task key={task.key} text={task.text} id={task.id}
                        removeTask={this.props.removeTask}  editTask={this.props.editTask}/>) }
            </div>

        </div>
        );
    };


   }

    


export default Task_list;