import React, { Component, createRef } from 'react';

class Add_task extends Component {


    constructor(props){
        super(props);
        this.state = { tasksCount: 0 };

        this.addItem = this.addItem.bind(this);
    }

    addItem(e) {
        if (this._inputElement.value !== "") {
            var newItem = {
              text: this._inputElement.value,
              id: this.state.tasksCount,
              key: Date.now()
            };

            this.props.addTask(newItem);
            

            this.state.tasksCount++;
            this._inputElement.value = "";
            console.log(JSON.stringify({ newItem}));
        }
             
        e.preventDefault();
        
    }
    
    

    render() {
        return (
            <div>
                <form onSubmit={this.addItem}>
                    <input  placeholder='Add Task' ref={(a) => this._inputElement = a} />
                    <button  type='submit'>Add</button>
                </form>

                
            </div>
            
        );
 }
}

export default Add_task;