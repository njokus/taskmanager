import React, { Component } from 'react';

class Task extends Component {

    constructor(props){
        super(props);

        this.removeTask = this.removeTask.bind(this);
        this.markDone = this.markDone.bind(this);
        this.editTask = this.editTask.bind(this);

        this.state = {
            backgroundColor: ''
        }
    }

    removeTask(){
        this.props.removeTask(this.props.id);
    }
    
    editTask(){
        this.props.editTask(this.props.id);
    }
    
    markDone(){
        if (this.state.backgroundColor === ''){
            this.setState({ backgroundColor: 'red' });
        } else {
            this.setState({backgroundColor: ''});
        }
    }

    render() {
        return (
            <div>
                   <div  style={{backgroundColor: this.state.backgroundColor,display:'flex'
                   ,justifyContent:'space-between',border:'3px solid green',
                   width:'550px',margin:'37px',padding:'5px'
                   ,overflow:'auto'}}>

                    <div style={{marginTop:'-2px',backgroundColor:'blue',width:'45%',height:'53px',
                     overflow:'auto',border:'3px solid yellow'}} >
                        <h5 style={{color:'white',marginTop:'2px'}} >{this.props.text.toUpperCase()}</h5>
                    </div>
                    <div>
                         <button style={{backgroundColor:'orange',color:'whiteksmoke'}} 
                         onClick={this.editTask}>modifier
                         </button>
                    </div>
                    <div >
                        <button style={{backgroundColor:'purple',
                        textDecorationColor:'Highlight',color:'whitesmoke'}} title='cliquer pour changer le statut'
                         onClick={this.markDone}>Finaliser/Activer
                         </button>
                    </div>
                    <div>
                         <button style={{backgroundColor:'red',color:'whitesmoke'}} 
                         onClick={this.removeTask}>Supprimer
                         </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Task;